#gnuplot -e "filename='Pos_000950000.dat'" test.gp
#set terminal postscript eps color enhanced font "Times-Roman" 20
#set output "filename.eps"
set terminal png size 2400,1800 enhanced font 'Times-Roman,36'
set output 'filename.png'


# -------------------------------- Eigenvalues -------------------------------- 

#set title 'RBC'
set xlabel 'x'
set ylabel 'y' rotate by 90
set xrange[-1.1:1.1]
set yrange[-1.1:1.1]
plot filename using 2:3 notitle with lp pt 5 axes x1y1
