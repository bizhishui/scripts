#!/bin/bash

#bash used to call gnuplot plot a serial figures

rm *.png
for s in $@; do
    echo $s
    gnuplot -e "filename='$s'" `dirname $0`/xyline.gp
    filename2="${s/dat/png}"
    mv filename.png $filename2
done
