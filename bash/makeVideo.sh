#!/bin/bash

#bash used to convert figures to video

if [ $# -eq 1 ]; then
    ffmpeg -framerate $1 -pattern_type glob -i '*.png' -c:v libx264 -pix_fmt yuv420p rbc2d.mp4
else
    echo "This script need ONE parameter:"
    echo "    -: the frame rate,"
    exit 1
fi
